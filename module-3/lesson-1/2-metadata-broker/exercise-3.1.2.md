# Excercise 3.1.1 - Configure and deploy the Metadata Broker

This exercise is split up into two tasks, one of the two should be executed. Task 1a aims at Kubernetes deployments of the Metadata Broker, while Task 2a aims at Docker-compose deployments of the Metadata Broker.

## Task 1a - Configure and deploy the Metadata Broker (Kubernetes)
1. Deploy the Triple Store database used for storing the self-descriptions of connectors in a triple format. Which allows for querying the self-descriptions via SPARQL.
    ```
    helm repo add tsg https://nexus.dataspac.es/repository/tsg-helm
    helm upgrade --install -n default \
        fuseki tsg/fuseki \
        --set image=docker.nexus.dataspac.es/fuseki:multi-arch \
        --set password=changeme \
        --version 1.1.0
    ```
2. Create the IDS identity secret for the Metadata Broker. Used for requesting Dynamic Attribute Tokens at the DAPS. Change the paths in the command to reflect your folder structure:
    ```
    kubectl create secret generic \
        -n default \
        broker-ids-identity-secret \
        --from-file=ids.crt=../1-identity-provider/certificates/connectors/TSG-Testbed-Metadata-Broker/ids.crt \
        --from-file=ids.key=../1-identity-provider/certificates/connectors/TSG-Testbed-Metadata-Broker/ids.key \
        --from-file=ca.crt=../1-identity-provider/certificates/ca/cachain.crt
    ```
3. Modify the `values.yaml` file to reflect the configuration created in the previous task. The following fields should normally be changed:
    1. Insert the Connector identifier (`ids.info.idsid`) corresponding to the IDS identity secret created before. Also insert this identifier in the Broker configuration field (`ids.broker.id`)
    2. Insert the Participant identifier for both the `curator` and `maintainer` (`ids.info.curator` & `ids.info.maintainer`)
    3. Modify the API key (`ids.security.apiKeys[0].key` & `containers[0].apiKey`) to a private API key.
        > _Note_: The API key must start with `APIKEY-`
4. Deploy the Connector Helm chart with the Metadata Broker data app
    ```
    helm upgrade --install -n default broker tsg/tsg-connector --version 3.2.2 -f values.broker.yaml
    ```


## Task 1b - Configure and deploy the Metadata Broker (Docker-compose)
0. Make sure you already have an Docker network named `ids_dev` created in the previous exercise.
    ```
    docker network inspect ids_dev
    ```
1. Update the configuration values in the `docker-compose.yaml` file:
    1. The Fuseki administrative password (`ADMIN_PASSWORD`) must be changed to a private password.
2. Update the files `docker-compose-conf/cc.yaml` & `docker-compose-conf/da.yaml`
    1. Insert the Connector identifier (`info.idsid` @ `cc.yaml`) corresponding to the IDS identity secret created before. Also insert this identifier in the Broker configuration field (`broker.id` @ `cc.yaml`). In the data app configuration this must also be updated (`ids.connector-id` @ `da.yaml`)
    2. Insert the Participant identifier for both the `curator` and `maintainer` (`info.curator` & `info.maintainer` @ `cc.yaml`). Also update the participant identifier in the data app configuration (`ids.participant-id` @ `da.yaml`)
    3. The administrator password for the Broker (`users.admin` @ `cc.yaml`) should be updated to a new BCrypt encoded password.
    4. More information on the configuration of the core container YAML (`cc.yaml`) can be found at the [TSG Documentation](https://tno-tsg.gitlab.io/docs/core-container/configuration/) repository
    5. More information on the configuration of the Broker data app YAML (`da.yaml`) can be found at the [Broker Data App](https://gitlab.com/tno-tsg/broker/data-app) repository.
4. Deploy the Docker-compose configuration on your machine
    ```
    docker-compose up -d
    ```
