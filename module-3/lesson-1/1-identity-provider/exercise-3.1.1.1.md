# Excercise 3.1.1.1 - Generate certificate structure

## Task 1 - Create configuration for the PKI generator

1. Copy the example `pki.sample.yaml` to `pki.yaml`
2. Configure global defaults
    1. Provide global information used in all the certificates in the `global` key.  
        Fill `country`, `state`, `locality`, `organization`, `organizationalUnit` with the respective information.
        > _Note_: The country codes should follow the ISO 3166-1 standard
    2. Provide global contact information used as default for the participant & device entries in the DAPS in the `globalContact` key.
3. Configure the Certificate Authorities
    1. Root Certificate Authority
        1. Provide a common name for the Root CA in the `rootCa.commonName` key.
        2. (Optionally) Change the validity period of the CA certificate
            > _Note_: The validity should be at least twice the validity of the certificates that it will sign
        3. (Optionally) Provide information different from the global configuration. The following keys are available within the `rootCa` key:  
            `country`, `state`, `locality`, `organization`, `organizationalUnit`, `subjectAlternativeNames`
    2. Participant Sub-Certificate Authority
        1. Provide a common name for the Participant Sub-CA in the `participantSubCa.commonName` key.
        2. (Optionally) Change the validity period of the CA certificate
            > _Note_: The validity should be at least twice the validity of the certificates that it will sign
        3. (Optionally) Provide information different from the global configuration. The following keys are available within the `participantSubCa` key:  
            `country`, `state`, `locality`, `organization`, `organizationalUnit`, `subjectAlternativeNames`
    3. Device Sub-Certificate Authority
        1. Provide a common name for the Device Sub-CA in the `deviceSubCa.commonName` key.
        2. (Optionally) Change the validity period of the CA certificate
            > _Note_: The validity should be at least twice the validity of the certificates that it will sign
        3. (Optionally) Provide information different from the global configuration. The following keys are available within the `deviceSubCa` key:  
            `country`, `state`, `locality`, `organization`, `organizationalUnit`, `subjectAlternativeNames`
4. Configure the DAPS certificate
    1. Provide a common name for the DAPS in the `daps.name` key.
    2. (Optionally) Modify the Kubernetes namespace the DAPS will be deployed in.
        > _Note_: Only useful when deploying on a Kubernetes cluster
5. Create participant configuration. At least one participant must be created, which can be used for all connectors. Or separate participants can be created for the connectors. For the latter case, repeat the sub-steps below for each participant.
    1. Provide an identifier for the participant. This must be an URI.
        > _Note_: In the remainder of the module identifiers will be used in the form of URNs with the following prefix: `urn:ids:tsg:euh4d:training:`
    2. Provide a title of the participant.
    3. (Optionally) Override global configuration of the contact information (in `contact`) or certificate details (in `certificate`)
    4. (Optionally) Override the directory the certificate is stored in.
    5. (Optionally) Override the certification level for this participant.
6. Create connector configurations. For this training, at least 3 connectors should be created. For the Metadata Broker, a provider, and a consumer. More can be added for a larger demo environment. Repeat the sub-steps below for each connector 
    1. Provide an identifier for the connector. This must be an URI.
        > _Note_: In the remainder of the module identifiers will be used in the form of URNs with the following prefix: `urn:ids:tsg:euh4d:training:`
    2. Provide the participant identifier for the connector. This participant identifier must be created in the previous step (Step 4).
    3. Provide a title of the connector.
    4. (Optionally) Override global configuration of the contact information (in `contact`) or certificate details (in `certificate`)
    5. (Optionally) Override the directory the certificate is stored in.
    5. (Optionally) Override the security profile for this connector.
    6. (Optionally) Modify the Kubernetes namespace and override the secret name used to store the 
        > _Note_: Only useful when deploying on a Kubernetes cluster. The combination of `namespace` and `secretName` _**must**_ be unique.

## Task 2 - Execute the PKI generator
1. Open a terminal in the folder where the `pki.yaml` file is located
2. Execute the Docker run command below to create the full structure of the PKI:
    ```
    docker run -ti --rm -v $(pwd):/output docker.nexus.dataspac.es/tsg-pki-generator:main
    ```
    This Docker image contains the application that will convert the PKI YAML configuration into a folder structure.
3. Validate the generated folder structure. If the directories in the participant and connector configurations are not changed, the directory structure should look like (where `Participant-*` and `Connector-*` depend on the titles of the respective configurations):
    ```
    certificates
    |-- README.txt
    |-- ca
    |   |-- cachain.crt
    |   |-- deviceSubCa.crt
    |   |-- deviceSubCa.key
    |   |-- participantSubCa.crt
    |   |-- participantSubCa.key
    |   |-- rootCa.crt
    |   `-- rootCa.key
    |-- connectors
    |   |-- Connector-A
    |   |   |-- ids.crt
    |   |   `-- ids.key
    |   |-- Connector-B
    |   |   |-- ids.crt
    |   |   `-- ids.key
    |   `-- Connector-C
    |       |-- ids.crt
    |       `-- ids.key
    |-- daps
    |   |-- DAPS.crt
    |   |-- DAPS.key
    |   |-- components.yaml
    |   `-- participants.yaml
    `-- participants
        `-- Participant-A
            |-- ids.crt
            `-- ids.key
    ```
