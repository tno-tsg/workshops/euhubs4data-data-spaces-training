# Excercise 3.1.1.2 - Deploy Identity Provider

## Task 1 - Deploy the Dynamic Attribute Provisioning Service

For the deployment of the Dynamic Attribute Provisioning Service, you'll have to choose whether to use Kubernetes (preferred) or Docker-compose.

## Task 1a - Deploy the Dynamic Attribute Provisioning Service (Kubernetes)

1. Create a Kubernetes namespace where the DAPS will live. Optionally change `default` to a custom name for the namespace, but remember to update all of the commands below with that change.  
    ```
    kubectl create namespace default
    ```
2. Create the DAPS identity secret, containing the certificate, private key and CA chain, used for the signing of tokens:
    ```
    kubectl create secret generic \
        -n default \
        daps-identity \
        --from-file=daps.cert=./certificates/daps/DAPS.crt \
        --from-file=daps.key=./certificates/daps/DAPS.key \
        --from-file=cachain.cert=./certificates/ca/cachain.crt
    ```
3. Create the Certificate Authority secret, containing the certificate and private key for both the participant sub-CA and the device sub-CA. These are used to sign the certificate signing requests (CSRs) issued from the user interface
    ```
    kubectl create secret generic \
        -n default \
        daps-ca \
        --from-file=participantsubca.cert=./certificates/ca/participantSubCa.crt \
        --from-file=participantsubca.key=./certificates/ca/participantSubCa.key \
        --from-file=devicesubca.cert=./certificates/ca/deviceSubCa.crt \
        --from-file=devicesubca.key=./certificates/ca/deviceSubCa.key
    ```
4. Create the initial state config map, containing the configurations of the participants and components. Making sure that the membership and certification level/security profile are present in the DAPS.
    ```
    kubectl create configmap \
        -n default \
        daps-init \
        --from-file=participants.yaml=./certificates/daps/participants.yaml \
        --from-file=components.yaml=./certificates/daps/components.yaml
    ```
5. Deploy a MongoDB instance, the minimum required configuration is shown below. For more information and a list of all configuration possibilites visit the [MongoDB Helm chart](https://artifacthub.io/packages/helm/bitnami/mongodb) from Bitnami.  
    The `auth.rootPassword` should be changed to a private password
    ```
    helm upgrade --install -n default mongodb \
        bitnami/mongodb \
        --version 13.15.1 \
        --set auth.rootPassword=CHANGE \
        --set architecture=standalone
    ```
    > _Note_: When running on an ARM-based machine (e.g. Apple M1/M2) the MongoDB chart should be adapted to use non-Bitnami images:  
    > ```
    > helm upgrade --install mongodb bitnami/mongodb \
    >     --set image.repository=mongo \
    >     --set image.tag=latest \
    >     --set persistence.mountPath=/data/db \
    >     --set-json extraEnvVars='[{"name": "MONGO_INITDB_ROOT_USERNAME","value":"root"},{"name": "MONGO_INITDB_ROOT_PASSWORD", "value": "CHANGE"}]'
    > ```
6. Update the configuration values for the [DAPS Helm Chart](https://gitlab.com/tno-tsg/helm-charts/daps) in `k8s/values.yaml`. The following properties should/can be modified for this course:
    1. The administrator password in `adminPassword` should be updated to a new BCrypt encoded password.
    2. The MongoDB password (`mongodb.password`) must match the password set for the MongoDB deployment in the step before.
    3. More details on the configuration can be found in the [DAPS Helm Chart](https://gitlab.com/tno-tsg/helm-charts/daps) repository

7. Install the Helm chart on your cluster:
    ```
    helm repo add tsg https://nexus.dataspac.es/repository/tsg-helm
    helm upgrade --install -n default daps \
        tsg/daps \
        --version 1.1.3 \
        -f values.yaml
    ```

## Task 1b - Deploy the Dynamic Attribute Provisioning Service (Docker-compose)
1. Create a Docker network used across this training
    ```
    docker network create ids_dev
    ```
2. Update the configuration values in the `docker-compose.yaml` file:
    1. The MongoDB password (`MONGO_INITDB_ROOT_PASSWORD`) must be changed to a private password.
    2. The administrator password (`ADMIN_PASSWORD`) should be updated to a new BCrypt encoded password.
    3. More details on the configuration can be found in the [DAPS Helm Chart](https://gitlab.com/tno-tsg/helm-charts/daps) repository

3. Deploy the Docker-compose configuration on your machine
    ```
    docker-compose up -d
    ```


## Task 2 - Access the DAPS interface
1. Navigate to [http://localhost:30980](http://localhost:30980) to view the DAPS Web UI.
2. In the `Participants` and `Connectors` tabs, you can view the existing entries that are present inside this DAPS instance.
3. For the `Management` tab, you can login with email `noreply@dataspac.es` and the password that you've set in `adminPassword` or `ADMIN_PASSWORD`
