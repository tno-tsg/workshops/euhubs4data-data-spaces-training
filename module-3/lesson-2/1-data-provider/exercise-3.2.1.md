# Excercise 3.2.1 - Configure and deploy the Data Provider

This exercise is split up into two tasks, one of the two should be executed. Task 1a aims at Kubernetes deployments of the Data Provider, while Task 2a aims at Docker-compose deployments of the Data Provider.

After the deployment of the Data Provider, task 2 aims at creating a resource with an attached policy that can be retrieved by a Data Consumer.

## Task 1a - Configure and deploy the Data Provider (Kubernetes)
1. Create the IDS identity secret for the Data Provider. Used for requesting Dynamic Attribute Tokens at the DAPS. Change the paths in the command to reflect your folder structure:
    ```
    kubectl create secret generic \
        -n default \
        provider-ids-identity-secret \
        --from-file=ids.crt=../../lesson-1/1-identity-provider/certificates/connectors/Data-Provider/ids.crt \
        --from-file=ids.key=../../lesson-1/1-identity-provider/certificates/connectors/Data-Provider/ids.key \
        --from-file=ca.crt=../../lesson-1/1-identity-provider/certificates/ca/cachain.crt
    ```
2. Modify the `values.yaml` file to reflect the configuration created in the previous task. The following fields should normally be changed:
    1. Insert the Connector identifier (`ids.info.idsid`) corresponding to the IDS identity secret created before. Also insert this identifier in the Broker configuration field (`ids.broker.id`)
    2. Insert the Participant identifier for both the `curator` and `maintainer` (`ids.info.curator` & `ids.info.maintainer`)
    3. Modify the API key (`ids.security.apiKeys[0].key` & `containers[0].apiKey`) to a private API key.
        > _Note_: The API key must start with `APIKEY-`
3. Deploy the Connector Helm chart with the Data Provider data app
    ```
    helm upgrade --install -n default data-provider tsg/tsg-connector --version 3.2.2 -f values.yaml
    ```


## Task 1b - Configure and deploy the Data Provider (Docker-compose)
0. Make sure you already have an Docker network named `ids_dev` created in the previous exercise.
    ```
    docker network inspect ids_dev
    ```
1. Update the configuration of the Core Container `docker-compose-conf/cc.yaml`
    1. Insert the Connector identifier (`info.idsid`) corresponding to the IDS identity secret created before.
    2. Insert the Participant identifier for both the `curator` and `maintainer` (`info.curator` & `info.maintainer`).
    3. Insert the Broker identifier (`broker.id`), use the identifier of the Broker as configured in 3.1.2, the Broker address (`broker.address`) should reflect the endpoint of the broker (defaults to `http://broker-tsg-connector:8080/infrastructure`).
    4. The administrator password for the Broker (`users.admin`) should be updated to a new BCrypt encoded password.
    5. More information on the configuration of the core container YAML (`cc.yaml`) can be found at the [TSG Documentation](https://tno-tsg.gitlab.io/docs/core-container/configuration/) repository
3. Deploy the Docker-compose configuration on your machine
    ```
    docker-compose up -d
    ```

## Task 2 - Create and publish a resource
0. The user interface is by default reachable at: [http://localhost:31080](http://localhost:31080)
1. Login with the credentials set in the `users.admin` field, where the ID is the username and the password the decrypted BCrypt password.
2. Navigate to `Artifact Handling > Provider`, or click [here](http://localhost:31080/#/artifacts/provider)
3. Upload a new artifact
    1. Provide a title and description for the artifact you're uploading
    2. Select a file from your machine to be used for the artifact
    3. Keep the `Artifact ID` empty, a unique identifier will be automatically created
    4. Click on the `Generic read access` button to generate a policy that allows `READ` and `USE` actions for any connector with a valid entry in the identity provider
4. Verify the resource is inserted into the self-description of the connector by navigating to `Self Description > Request`, or click [here](http://localhost:31080/#/selfdescription/request)
    1. Fill in the Connector identifier used in the previous task.
    2. Set the Access URL to: `http://localhost:8080/router/artifacts`.
    3. Leave `Agent ID` and `Requested Element` empty.
    4. Click on `Request description`.
    5. Validate that in the response at least one catalog is present.
    6. Click on the catalog and validate that there is an entry with the title and description set in the previous step.
