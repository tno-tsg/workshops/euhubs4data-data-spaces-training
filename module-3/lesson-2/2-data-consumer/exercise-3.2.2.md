# Excercise 3.2.2 - Configure and deploy the Data Consumer

This exercise is split up into two tasks, one of the two should be executed. Task 1a aims at Kubernetes deployments of the Data Consumer, while Task 2a aims at Docker-compose deployments of the Data Consumer.

After the deployment of the Data Consumer, task 2 aims at creating a resource with an attached policy that can be retrieved by a Data Consumer.

## Task 1a - Configure and deploy the Data Consumer (Kubernetes)
1. Create the IDS identity secret for the Data Provider. Used for requesting Dynamic Attribute Tokens at the DAPS. Change the paths in the command to reflect your folder structure:
    ```
    kubectl create secret generic \
        -n default \
        provider-ids-identity-secret \
        --from-file=ids.crt=../../lesson-1/1-identity-provider/certificates/connectors/Data-Provider/ids.crt \
        --from-file=ids.key=../../lesson-1/1-identity-provider/certificates/connectors/Data-Provider/ids.key \
        --from-file=ca.crt=../../lesson-1/1-identity-provider/certificates/ca/cachain.crt
    ```
2. Modify the `values.yaml` file to reflect the configuration created in the previous task. The following fields should normally be changed:
    1. Insert the Connector identifier (`ids.info.idsid`) corresponding to the IDS identity secret created before. Also insert this identifier in the Broker configuration field (`ids.broker.id`)
    2. Insert the Participant identifier for both the `curator` and `maintainer` (`ids.info.curator` & `ids.info.maintainer`)
    3. Modify the API key (`ids.security.apiKeys[0].key` & `containers[0].apiKey`) to a private API key.
        > _Note_: The API key must start with `APIKEY-`
4. Deploy the Connector Helm chart with the Data Consumer data app
    ```
    helm upgrade --install -n default data-consumer tsg/tsg-connector --version 3.2.2 -f values.yaml
    ```


## Task 1b - Configure and deploy the Data Consumer (Docker-compose)
0. Make sure you already have an Docker network named `ids_dev` created in the previous exercise.
    ```
    docker network inspect ids_dev
    ```
1. Update the configuration of the Core Container `docker-compose-conf/cc.yaml`
    1. Insert the Connector identifier (`info.idsid`) corresponding to the IDS identity secret created before.
    2. Insert the Participant identifier for both the `curator` and `maintainer` (`info.curator` & `info.maintainer`).
    3. Insert the Broker identifier (`broker.id`), use the identifier of the Broker as configured in 3.1.2, the Broker address (`broker.address`) should reflect the endpoint of the broker.
    4. The administrator password for the Broker (`users.admin`) should be updated to a new BCrypt encoded password.
    5. More information on the configuration of the core container YAML (`cc.yaml`) can be found at the [TSG Documentation](https://tno-tsg.gitlab.io/docs/core-container/configuration/) repository
3. Deploy the Docker-compose configuration on your machine
    ```
    docker-compose up -d
    ```

## Task 2 - Create and publish a resource
0. The user interface is by default reachable at: [http://localhost:31081](http://localhost:31081)
1. Login with the credentials set in the `users.admin` field, where the ID is the username and the password the decrypted BCrypt password.
2. Navigate to `Self Description > Request`, or click [here](http://localhost:31080/#/selfdescription/request)
    1. Fill in the Connector identifier used for the Data Provider.
    2. Set the Access URL to: `http://data-provider-tsg-connector:8080/router/artifacts`.
    3. Leave `Agent ID` and `Requested Element` empty.
    4. Click on `Request description`.
    5. Validate that in the response at least one catalog is present.
    6. Click on the catalog and validate that there is an entry with the title and description set in the previous exercise.
    7. (Optionally) View the artifact metadata or the contract offer by clicking on the icons under `Info`.
    8. Click on the lock icon under actions to navigate to the artifact consumer view with the contract request form (the second form on the page) with the information of the resource.
    9. The `Connector ID`, `Agent ID`, `Access URL`, and `Contract Offer` should now be filled with the data that has been retrieved from the Self-Description request.
    10. Click on `Request Contract`.
    11. If everything succeeded, the `Transfer Contract` field in the Request artifact form should be filled with an URI.
    12. Verify that the form is filled with the right information.
    13. Click on `Request artifact` to retrieve the artifact on the connector, which will present this to the interface as download.

Optionally, to use the Metadata Broker for finding the Data Provider:
* Retrieve the Connector ID and Access URL of the Provider via a Query to the broker
    1. Navigate to `Self Description > Query Message`, or click [here](http://localhost:31081/#/selfdescription/querymessage)
    2. Make sure the `Connector ID`, `Agent ID`, `Access URL`, `Query Language`, `Query Scope` fields are empty, the `Recipient Scope` is `ANY`. And the `Query` field can be:
        ```
        PREFIX ids: <https://w3id.org/idsa/core/>
        SELECT ?connectorId ?accessURL
        WHERE {
            GRAPH ?g {
                ?connectorId ids:hasDefaultEndpoint/ids:accessURL ?accessURL.
            }
        }
        ```
        This query will retrieve all Connector IDs together with their default access URL
