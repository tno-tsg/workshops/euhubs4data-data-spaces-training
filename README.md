# EUHubs4Data Data Spaces Training

This repository contains the exercises for the EUHubs4Data Data Spaces Training, particularely for Module 3 of the training. This module allows participants of the training to actively deploy a test data space on their local machine using components from the [TNO Security Gateway](https://tno-tsg.gitlab.io/).

The folder structure directs you in a logical way through all the steps required for deploying the technical side of a data space: the identity provider, the metadata broker, a data provider, and a data consumer.

Participants can choose to either use Kubernetes or Docker Compose for executing the exercises. The Kubernetes variant is aimed at local kubernetes clusters, e.g. via Docker Desktop.

## Prerequisites
### General
- Linux/Unix based terminal (Linux, MacOS, Windows WSL)
- At least 4GB of available RAM (make sure this is available to Docker/Kubernetes)

### Kubernetes
- kubectl (https://kubernetes.io/docs/tasks/tools/)
- helm (https://helm.sh/docs/intro/install/)
- Recent Kubernetes cluster (version >= 1.24)

## Cleanup

For cleaning up all resources of the module choose either the Kubernetes or Docker Compose section below.

### Kubernetes & Helm

Uninstall all Helm releases created in the training and subsequently delete all the created secrets and config maps.

```
helm uninstall -n default mongodb daps fuseki broker data-consumer data-provider
kubectl delete -n default secret/daps-ca secret/daps-identity secret/broker-ids-identity-secret secret/provider-ids-identity-secret secret/consumer-ids-identity-secret configmap/daps-init
```

### Docker Compose

Uninstall all docker compose releases

```
docker-compose -f module-3/lesson-2/2-data-consumer/docker-compose.yaml down
docker-compose -f module-3/lesson-2/1-data-provider/docker-compose.yaml down
docker-compose -f module-3/lesson-1/2-metadata-broker/docker-compose.yaml down
docker-compose -f module-3/lesson-1/1-identity-provider/docker-compose.yaml down
```
> _Note_: These commands assume the current directory is the root of this repository.

## Issues

Feel free to open an issue if some of the steps went wrong. 